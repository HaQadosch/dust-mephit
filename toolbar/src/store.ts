import { flow, makeAutoObservable } from 'mobx'

export interface IDoubler {
  value: number
  readonly double: number
  readonly state: symbol
  increment: () => void
  onAccept: () => void
}

const preview = Symbol.for('state.preview')
const accepting = Symbol.for('state.accepting')
const onCall = Symbol.for('state.onCall')

export function createDoubler (value: number, status = preview) {
  return makeAutoObservable({
    value,
    status,
    get double () {
	    return this.value * 2
    },
    get state () {
	    return this.status
    },
    increment () {
	    this.value++
    },
    * onAccept () {
	    if (this.status === preview) {
        this.status = accepting
        console.log('onAccept')
        // try
        yield setTimeout(() => null, 1000)
        console.log('accepted')
        this.status = onCall
        // catch
	    }
    }
  })
}
