import React from 'react'
import {
  Toolbar,
  Accept,
  Skip,
  VoiceMail,
  Close
} from './features/toolbar/Toolbar'
import { createDoubler, IDoubler } from './store'
import './App.css'

export const StoreContext = React.createContext<IDoubler | null>(null)
StoreContext.displayName = 'store'

const AppStore: React.FC = ({ children }) => {
  const [doubler] = React.useState<IDoubler>(() => createDoubler(2))

  return (
    <StoreContext.Provider value={doubler}>
      {children}
    </StoreContext.Provider>
  )
}

const App: React.FC = () => {
  return (
    <div className='App'>
      <AppStore>
        <Toolbar>
          <Accept />
          <Skip />
          <VoiceMail />
          <Close />
        </Toolbar>
      </AppStore>
    </div>
  )
}

export default App
