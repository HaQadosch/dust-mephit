import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import '../App.css'
import {
  Toolbar,
  Accept,
  Skip,
  VoiceMail,
  Close
} from '../features/toolbar/Toolbar'
import { StoreContext } from '../App'
import { createDoubler } from '../store'

export default {
  title: 'Example/Toolbar',
  component: Toolbar,
  decorators: [
    Story => {
      const AppStore: React.FC = ({ children }) => {
        const [doubler] = React.useState(() => createDoubler(2))

        return (
          <StoreContext.Provider value={doubler}>
            {children}
          </StoreContext.Provider>
        )
      }
      return (
        <AppStore>
          <Story />
        </AppStore>
      )
    }
  ]
} as ComponentMeta<typeof Toolbar>

const Template: ComponentStory<typeof Toolbar> = (args) => (
  <Toolbar {...args}>
    <Accept />
    <Skip />
    <VoiceMail />
    <Close />
  </Toolbar>
)

export const Default = Template.bind({})
Default.args = {
  size: 'large'
}

export const Large = Template.bind({})
Large.args = {
  size: 'large'
}

export const Middle = Template.bind({})
Middle.args = {
  size: 'middle'
}

export const Small = Template.bind({})
Small.args = {
  size: 'small'
}

export const Preview = Template.bind({})
Preview.args = {
  size: 'large'
}
export const onCall = Template.bind({})
onCall.args = {
  size: 'large'
}
