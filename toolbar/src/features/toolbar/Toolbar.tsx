import React from 'react'
import { Button } from 'antd'
import {
  CheckCircleTwoTone,
  RightCircleTwoTone,
  PlaySquareTwoTone,
  CloseCircleTwoTone
} from '@ant-design/icons'
import { StoreContext } from '../../App'
import { IDoubler } from '../../store'
import { observer } from 'mobx-react-lite'
import { flowResult } from 'mobx'

export type SizeType = 'small' | 'middle' | 'large' | undefined
interface IToolbar {
  /*
   * Size of the toolbar, all buttons will inherit the same size
   */
  size?: SizeType
}

const ToolbarContext = React.createContext<SizeType>('large')
ToolbarContext.displayName = 'Toolbar'

export const Toolbar: React.FC<IToolbar> = ({ children, size = 'large' }) => {
  return (
    <ToolbarContext.Provider value={size}>
      {children}
    </ToolbarContext.Provider>
  )
}

export const Accept: React.FC = observer(() => {
  const size = React.useContext(ToolbarContext)
  const appStore = React.useContext(StoreContext)

  return (
    <Button
      type='text'
      icon={<CheckCircleTwoTone twoToneColor='#52c41a' />}
      size={size}
      disabled={appStore?.state !== Symbol.for('state.preview')}
      onClick={onClickAccept(appStore)}
    >Accept {appStore?.double}
    </Button>
  )
})

const onClickAccept = (store: IDoubler | null) => (evt: React.MouseEvent<HTMLElement>): void => {
  console.log({ onClickAccept: evt })
  flowResult(store?.onAccept())
}

export const Skip: React.FC = () => {
  const size = React.useContext(ToolbarContext)
  const appStore = React.useContext(StoreContext)

  return (
    <Button
      type='text'
      icon={<RightCircleTwoTone />}
      size={size}
      onClick={evt => onClickSkip(appStore, evt)}
    >Skip
    </Button>
  )
}

function onClickSkip (store: IDoubler | null, evt: React.MouseEvent<HTMLElement>): void {
  console.log({ onClickSkip: evt })
  store?.increment()
}

export const VoiceMail: React.FC = () => {
  const size = React.useContext(ToolbarContext)

  return (
    <Button
      type='text'
      icon={<PlaySquareTwoTone />}
      size={size}
      onClick={onClickVoicemail}
    >Voicemail
    </Button>
  )
}

function onClickVoicemail (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({ onClickVoicemail: evt })
}

export const Close: React.FC = () => {
  const size = React.useContext(ToolbarContext)

  return (
    <Button
      type='text'
      icon={<CloseCircleTwoTone twoToneColor='#eb2f96' />}
      size={size}
      onClick={onClickClose}
    >Close
    </Button>
  )
}

function onClickClose (evt: React.MouseEvent<HTMLButtonElement>): void {
  console.log({ onClickClose: evt })
}
